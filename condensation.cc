#include <cstdlib>
#include <vector>
#include <ctime>

#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "condensation_utils.h"


int main (int argc, char **argv) {
    CondensationConfig conf = getCondensationConfig(argc, argv);

    srand(1);

    cv::VideoCapture cap;
    if(conf.useDevice)
        cap = cv::VideoCapture(conf.srcdevice);
    else
        cap = cv::VideoCapture(conf.srcpath);

    cv::Size frameSize = getFrameSize(cap);

    int framerate = cap.get(cv::CAP_PROP_FPS);
    int delay = (int) (1000.0 / framerate);
    //int delay = 1;

    std::vector<Particle> particles = randomParticles(frameSize, conf.n_particles);

    cv::Mat avg, std;
    obtainInitialModel(cap, avg, std, conf.n_learning_frames);

    cv::Mat currentFrame, bwFrame, floatFrame, diff, diffImg;
    clock_t frameBegin, frameEnd;
    cv::Mat particleImg(frameSize, CV_8UC1);

    while(cap.grab()) {
        frameBegin = clock();
        particleImg = cv::Mat::zeros(frameSize, CV_8UC1);
        for (auto particle : particles) {
            cv::Point2f center(particle.col, particle.row);
            cv::circle(particleImg, center, 2, cv::Scalar(255), cv::FILLED);
        }
        cv::imshow("Particles", particleImg);

        cap.retrieve(currentFrame);

        // Perform observation
        updateConfidence(currentFrame, avg, std, particles);
        particles = conf.resampleAlg(particles, conf.keepRatio, frameSize);
        disturbParticles(particles, conf.disturb_scale, frameSize);

        ObjPosition objPos = objectPosition(particles);
        cv::ellipse(currentFrame, objPos, cv::Scalar(0,0,255), 2);
        cv::imshow("Video", currentFrame);

        frameEnd = clock();

        int elapsed_ms = (frameEnd - frameBegin) / (CLOCKS_PER_SEC / 1000);
        int compensation = delay - elapsed_ms;
        char c = cv::waitKey((compensation > 0)? compensation : 1);
        if(c == 27) break;
    }

    return EXIT_SUCCESS;
}
