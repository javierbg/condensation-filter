#include "condensation_utils.h"

#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

cv::Size getFrameSize(const cv::VideoCapture &cap) {
    return cv::Size((int) cap.get(cv::CAP_PROP_FRAME_WIDTH),
                    (int) cap.get(cv::CAP_PROP_FRAME_HEIGHT));
}

void obtainInitialModel(cv::VideoCapture &cap, cv::Mat &avg, cv::Mat &std, int n_frames) {
    cv::Size frameSize = getFrameSize(cap);
    cv::Mat currentFrame, yuvFrame, floatFrame;
    avg = cv::Mat::zeros(frameSize, CV_32FC3);
    std = cv::Mat::zeros(frameSize, CV_32FC3);
    for(int i=0 ; i < n_frames ; ++i) {
        if (cap.grab()){
            cap.retrieve(currentFrame);
            cv::cvtColor(currentFrame, yuvFrame, cv::COLOR_BGR2YUV);
            yuvFrame.convertTo(floatFrame, CV_32FC3);
            cv::accumulate(floatFrame, avg);
            cv::accumulateSquare(floatFrame, std);
        }
        else {
            std::cerr << "Not enough learning frames in source video" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    avg /= (float) n_frames;
    std /= (float) n_frames;
    std -= avg.mul(avg);
    cv::sqrt(std, std);
}

float randomF(float min, float max) {
    float range = max - min;
    float uniform = float(rand()) / float(RAND_MAX);
    return min + range * uniform;
}

std::vector<Particle> randomParticles(const cv::Size &frame_dims, size_t n_particles) {
    std::vector<Particle> result;

    float max_row = frame_dims.height;
    float max_col = frame_dims.width;

    for(int i=0 ; i < n_particles ; ++i) {
        Particle p;
        p.row = randomF(0, max_row);
        p.col = randomF(0, max_col);
        p.confidence = 1.0;
        result.push_back(p);
    }

    return result;
}

ObjPosition objectPosition(const std::vector<Particle> &particles, float sigmaRadius) {
    // Mean of all points, weighted on their confidence
    float x, y, stdx, stdy;
    float confidenceSum = 0.0;

    for (auto particle : particles) {
        y += particle.row * particle.confidence;
        x += particle.col * particle.confidence;
        stdy += particle.row * particle.row * particle.confidence;
        stdx += particle.col * particle.col * particle.confidence;
        confidenceSum += particle.confidence;
    }
    x /= confidenceSum;
    y /= confidenceSum;

    stdx /= confidenceSum;
    stdx -= x*x;
    stdx = cv::sqrt(stdx) * sigmaRadius;

    stdy /= confidenceSum;
    stdy -= y*y;
    stdy = cv::sqrt(stdy) * sigmaRadius;

    cv::RotatedRect rr(cv::Point2f(x, y), cv::Size2f(stdx, stdy), 0);
    return rr;
}

void updateConfidence(const cv::Mat &currentFrame, const cv::Mat &avg, const cv::Mat &std, std::vector<Particle> &particles) {
    cv::Mat yuvFrame, floatFrame, diffFrame;
    cv::cvtColor(currentFrame, yuvFrame, cv::COLOR_BGR2YUV);
    yuvFrame.convertTo(floatFrame, CV_32FC3);
    cv::absdiff(floatFrame, avg, diffFrame);
    //cv::divide(diffFrame, std, diffFrame);

    std::transform(particles.begin(), particles.end(), particles.begin(),
    [&] (Particle particle) {
        cv::Vec3f val = diffFrame.at<cv::Vec3f>(particle.row, particle.col);
        cv::Vec3f deviation = std.at<cv::Vec3f>(particle.row, particle.col);
        for(int i=0 ; i < 3 ; ++i){
            deviation[i] = deviation[i] > 0.01? deviation[i] : 0.01;
            val[i] = val[i] / deviation[i];
        }
        //float difference = abs(val-255.0);

        // Avoid division by zero
        //particle.confidence = (difference < 0.01)? (1/0.01) : 1 / difference;
        particle.confidence = vecMagnitude(val);
        return particle;
    });
}

std::vector<Particle> resampleParticlesRoulette(const std::vector<Particle> &particles, float keepRatio, const cv::Size &frameSize) {
    std::vector<float> accConfidence;
    std::vector<Particle> newParticles;
    size_t nParticles = particles.size();
    size_t keepNumber = nParticles * keepRatio;
    size_t renewedNumber = nParticles - keepNumber;

    // Accumulate confidence
    float acc = 0.0;
    for (auto particle : particles) {
        acc += particle.confidence;
        accConfidence.push_back(acc);
    }

    // Perform roulette selection of particles
    for(size_t i=0 ; i < keepNumber ; ++i) {
        float roulette = randomF(0, acc);
        for (size_t j=0 ; j < nParticles ; ++j) {
            if (roulette < accConfidence[j]){
                newParticles.push_back(particles[j]);
                break;
            }
        }
    }

    // Take the rest as random
    std::vector<Particle> renewedParticles = randomParticles(frameSize, renewedNumber);
    newParticles.insert(newParticles.end(), renewedParticles.begin(), renewedParticles.end());

    return newParticles;
}

std::vector<Particle> resampleParticlesTournament(const std::vector<Particle> &particles, float keepRatio, const cv::Size &frameSize) {
    std::vector<Particle> newParticles;
    size_t nParticles = particles.size();
    size_t keepNumber = nParticles * keepRatio;
    size_t renewedNumber = nParticles - keepNumber;

    // Perform roulette selection of particles
    for(size_t i=0 ; i < keepNumber ; ++i) {
        Particle p1 = particles[rand() % nParticles];
        Particle p2 = particles[rand() % nParticles];

        if(p1.confidence > p2.confidence) newParticles.push_back(p1);
        else                              newParticles.push_back(p2);
    }

    // Take the rest as random
    std::vector<Particle> renewedParticles = randomParticles(frameSize, renewedNumber);
    newParticles.insert(newParticles.end(), renewedParticles.begin(), renewedParticles.end());

    return newParticles;
}

float boundValue(float value, float min, float max) {
    if (value < min) return min;
    else if (value > max) return max;
    else return value;
}

void disturbParticles(std::vector<Particle> &particles, float scale, cv::Size bounds) {
    std::transform(particles.begin(), particles.end(), particles.begin(),
    [scale, bounds] (Particle particle) {
        float r = randomF(0, scale);
        float theta = randomF(0, 2*M_PI);

        float newr = particle.row + r * cos(theta);
        float newc = particle.col + r * sin(theta);
        particle.row = boundValue(newr, 0, bounds.height);
        particle.col = boundValue(newc, 0, bounds.width);
        return particle;
    });
}

float vecMagnitude(const cv::Vec3f &vec) {
    float result;
    result  = vec[0]*vec[0];
    result += vec[1]*vec[2];
    result += vec[2]*vec[2];
    return cv::sqrt(result);
}

void errorArgumentos(std::string mensaje) {
    std::cerr << mensaje << std::endl;
    exit(EXIT_FAILURE);
}

CondensationConfig getCondensationConfig(int argc, char **argv) {
    CondensationConfig conf;
    cv::CommandLineParser parser(argc, argv, keys);
    if (!parser.check()) {
        parser.printErrors();
        exit(EXIT_FAILURE);
    }

    parser.about("condensation");
    if (parser.has("help")){
        parser.printMessage();
        exit(EXIT_SUCCESS);
    }

    conf.useDevice = parser.has("c");
    if (conf.useDevice) {
        conf.srcdevice = parser.get<int>(0);
    }
    else {
        conf.srcpath = parser.get<std::string>(0);
    }

    conf.n_learning_frames = parser.get<int>("l");
    if (conf.n_learning_frames <= 0) {
        errorArgumentos("Invalid number of learning frames");
    }

    conf.n_particles = parser.get<int>("p");
    if (conf.n_particles <= 0) {
        errorArgumentos("Invalid number of particles");
    }

    conf.disturb_scale = parser.get<float>("d");
    if (conf.disturb_scale < 0) {
        errorArgumentos("Invalid disturb scale");
    }

    std::string type = parser.get<std::string>("r");
    if (resampleTypes.count(type) > 0) {
        conf.resampleAlg = resampleTypes.at(type);
    }
    else {
        errorArgumentos("Invalid resample type, valid types are: \"roulette\", \"tournament\"");
    }

    conf.keepRatio = parser.get<float>("k");
    if(conf.keepRatio < 0.0 || conf.keepRatio > 1.0) {
        errorArgumentos("Invalid keep ratio, must be between 0 and 1");
    }

    return conf;
}
