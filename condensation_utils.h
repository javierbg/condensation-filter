#pragma once

#include <map>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>


typedef struct Particle {
    float row, col;
    float confidence;
} Particle;

typedef cv::RotatedRect ObjPosition;

typedef std::vector<Particle> (*ResampleFunction)(const std::vector<Particle> &particles, float keepRatio, const cv::Size &frameSize) ;

cv::Size getFrameSize(const cv::VideoCapture &cap);
void obtainInitialModel(cv::VideoCapture &cap, cv::Mat &avg, cv::Mat &std, int n_frames);
float randomF(float min, float max);
std::vector<Particle> randomParticles(const cv::Size &frame_dims, size_t n_particles);
ObjPosition objectPosition(const std::vector<Particle> &particles, float sigmaRadius=3.0);
void updateConfidence(const cv::Mat &currentFrame, const cv::Mat &avg, const cv::Mat &std, std::vector<Particle> &particles);
std::vector<Particle> resampleParticlesRoulette(const std::vector<Particle> &particles, float keepRatio, const cv::Size &frameSize);
std::vector<Particle> resampleParticlesTournament(const std::vector<Particle> &particles, float keepRatio, const cv::Size &frameSize);
void disturbParticles(std::vector<Particle> &particles, float scale, cv::Size bounds);
float vecMagnitude(const cv::Vec3f &vec);

const std::map<std::string, ResampleFunction> resampleTypes = {
    {"roulette", resampleParticlesRoulette},
    {"tournament", resampleParticlesTournament}
};

typedef struct CondensationConfig {
    bool useDevice;
    cv::String srcpath;
    int srcdevice;
    size_t n_learning_frames, n_particles;
    ResampleFunction resampleAlg;
    float disturb_scale, keepRatio;
} CondensationConfig;

const cv::String keys =
    "{help h usage ? |        | print this message      }"
    "{@videoin       | <none> | source video (index or path)}"
    "{c |  | if set, then videoin is treated as a camera index, otherwise as a path}"
    "{l | 300 | learning frames}"
    "{p | 500 | number of particles}"
    "{d | 40 | disturbance scale}"
    "{r | tournament | type of resample, must be \"roulette\" or \"tournament\"}"
    "{k | 0.9 | ratio of particles to keep from the original sample, the rest will be completely random.\n"
               "Values close to 0 make the algorithm less precise, but avoid falling into local optima}"
    ;

CondensationConfig getCondensationConfig(int argc, char **argv);
